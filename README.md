# Yabai Bot 
  
Yabai Bot - Just another Discord bot written in Python  

## Python Usage

Clone repo, create a python environment, pip install src/python/requirements.txt, and run src/python/main.py. You'll need to set some environment variables, or use a config file. There's a sample config file that you can use, but make sure that you don't commit it anywhere.  
  
Requires python >= 3.7.9 and sqlite3. You'll also need a discord bot user.  
  
Alternatively, you can pull a pre-built docker container from this project's registry and run it, passing in the appropriate environment variables listed below.

### Required Variables:

Check out `terraform\digitalocean\data\droplet-startup.sh`. All the required variables will be passed into the docker container as environment variables.  
  
## setup.ps1

I've included a powershell script to make local testing easier. It will build a docker container and run it with the required environment variables. Use it with `setup.ps1 -dbpath <db_filename> -guild <guild id> -token <discord token> -rcon_pass <password> -rcon_host <hostname>`  
  
## contributing

Contributions are welcome. Please feel free to fork the project and submit an MR. If you have an issue, or would like a feature and are not confident in your ability to implement it, please open an issue and I'll do my best to look into it.  
  
The primary libraries used are discord and discord_slash. Read up on them here:  
https://discordpy.readthedocs.io/en/stable/index.html  
https://discord-py-slash-command.readthedocs.io/en/latest/index.html  

## notes

Persistant data is supported. There is a storage volume mounted at /storage, and a sqlite3 database at /storage/data. See `src/python/database.py` for some examples. I would prefer that data storage take place in this database, if you need help with SQL let me know.