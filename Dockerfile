FROM debian:10
RUN apt update
RUN apt install -y python3.7-dev python3-pip
COPY ./src/python/requirements.txt .
RUN pip3 install -r requirements.txt
COPY ./src/python /application/
WORKDIR /application
ENTRYPOINT ["python3", "main.py"]
