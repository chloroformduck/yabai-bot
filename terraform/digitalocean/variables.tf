variable "image" {
  description = "The docker image to use on the droplet"
}

variable "rcon_host" {
    description = "minecraft rcon hostname"
    default = ""
    sensitive = true
}

variable "rcon_pass" {
    description = "minecraft rcon password"
    default = ""
    sensitive = true
}

variable "token" {
    description = "discord bot token"
    sensitive = true
}

variable "docker_registry" {
    description = "the registry to pull the docker image from"
    default = "registry.gitlab.com"
}

variable "docker_user" {
    description = "the user for the registry"
}

variable "docker_pass" {
    description = "the pass for the registry"
    sensitive = true
}

variable "ssh_keys" {
  description = "list of ssh keys to add to the instance"
}

variable "guilds" {
  description = "one or more guild ids that you want the slash commands to be available in. Single string, single integer, or comma separated string are supported."
}