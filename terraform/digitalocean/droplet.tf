resource "digitalocean_droplet" "bot" {
  image  = "docker-20-04"
  name   = "yabai-bot"
  region = "lon1"
  size   = "s-1vcpu-1gb"
  ssh_keys = var.ssh_keys
  user_data = data.template_cloudinit_config.droplet-userdata.rendered
  volume_ids = [
    digitalocean_volume.bot_storage.id
  ]
}

resource "digitalocean_volume" "bot_storage" {
  region                  = "lon1"
  name                    = "yabaivolume1"
  size                    = 16
  initial_filesystem_type = "ext4"
  description             = "external volume for yabai bot"
}