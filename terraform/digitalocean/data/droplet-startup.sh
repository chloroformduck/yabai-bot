#!/bin/sh

# export some stuff needed for the bot to work
export Rcon_host="${rcon_host}"
export Rcon_pass="${rcon_pass}"
export Token="${token}"
export Guilds=${guilds}
export Volume="${volume}"

/usr/bin/docker login "${docker_registry}" -u "${docker_user}" -p "${docker_pass}"

/usr/bin/docker run -d -v /mnt/$Volume:/storage -e db_path=/storage/data -e guilds=$Guilds -e minecraft_guilds=$Guilds -e credentials_token=$Token -e minecraft_host_name=$Rcon_host -e minecraft_rcon_pass=$Rcon_pass --name "YabaiBot" ${image}