data "template_file" "droplet-startup" {
  template = file("${path.module}/data/droplet-startup.sh")
  vars = {

    rcon_host = var.rcon_host
    rcon_pass = var.rcon_pass
    token = var.token
    docker_registry = var.docker_registry
    docker_user = var.docker_user
    docker_pass = var.docker_pass
    image = var.image
    volume = digitalocean_volume.bot_storage.name
    guilds = var.guilds

  }
}

data "template_cloudinit_config" "droplet-userdata" {
  gzip = false
  base64_encode = false

  part {
    content_type = "text/x-shellscript"
    filename = "10-droplet-userdata"
    content = "#!/bin/sh\necho -n '${base64gzip(data.template_file.droplet-startup.rendered)}' | base64 -d | gunzip | /bin/sh"
  }
}