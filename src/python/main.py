import logging
import discord
from discord_slash import SlashCommand
from discord.ext import commands
from config import ConfigReader
from database import DbHelper

logging.basicConfig(level=logging.INFO)

module_list = ["minecraft", "admin", "suggestions"]    

def main():
    logging.info("Gathering configs")
    config = ConfigReader()
    token = config.get("credentials_token")
    db_path = config.get("db_path")
    logging.info("Connecting to the database")
    db = DbHelper(db_path)
    logging.info("Ensuring db is initialized")
    db.initialize()

    guilds = [guild for guild in db.get_guilds()]

    # This sets the command prefix and enables slash commands
    bot = commands.Bot(command_prefix='!y ', intents=discord.Intents.default())
    slash = SlashCommand(bot, sync_commands=True, sync_on_cog_reload=True)

    # Just print something when it's ready
    @bot.event
    async def on_ready():
        print(f"{bot.user.name} is ready")

    @bot.event
    async def on_guild_join(guild):
        logging.info(f"Joined a new guild. Initializing in database")
        db.add_guild(guild.id)
        admin_roles = [role for role in guild.roles if role.permissions.administrator]
        db.set_admin_roles(guild.id, admin_roles)
        for extension in module_list:
            bot.reload_extension(extension)

    @bot.event
    async def on_guild_role_create(role):
        admins = db.get_admin_roles()
        if role.permissions.administrator:
            admins.append(role.id)
            db.set_admin_roles(admins)
            for extension in module_list:
                bot.reload_extension(extension)
    
    @bot.command(name='hello', help='Responds with "Hello!"')
    async def hello(ctx):
        await ctx.send(f'Hello, {ctx.author.name}!')

    @slash.slash(name="hello", guild_ids=guilds)
    async def _hello(ctx):
        await ctx.send(f'Hello, {ctx.author.name}!')

    @slash.slash(name="ffxiv", guild_ids=guilds)
    async def free_trial(ctx):
        await ctx.send("Did you know that the critically acclaimed MMORPG Final Fantasy XIV has a free trial, and includes the entirety of A Realm Reborn AND the award-winning Heavensward expansion up to level 60 with no restrictions on playtime? Sign up, and enjoy Eorzea today! https://secure.square-enix.com/account/app/svc/ffxivregister?lng=en-us")

    for extension in module_list:
        bot.load_extension(extension)
    bot.run(token)

if __name__ == '__main__':
    main()
