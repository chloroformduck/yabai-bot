import logging
import discord
from discord_slash import cog_ext
from discord_slash.context import SlashContext
from discord_slash.utils.manage_commands import create_choice, create_option
from discord_slash.utils.manage_commands import create_permission
from discord_slash.model import SlashCommandPermissionType
from discord.ext import commands
from config import ConfigReader
from database import DbHelper

logging.getLogger()

config = ConfigReader()
minecraft_guilds = config.get_guild_ids('minecraft_guilds')
db_path = config.get("db_path")
db = DbHelper(db_path)
guilds = [guild for guild in db.get_guilds()]
admins = {guildId: [create_permission(adminId, SlashCommandPermissionType.ROLE, True) for adminId in db.get_admin_roles(guildId)] for guildId in db.get_guilds()}

class Admin(commands.Cog):
	def __init__(self, bot: commands.Bot):
		self.bot = bot

	@cog_ext.cog_slash(
		name="set-channel",
		description="Set this channel as the channel for certain commands and features",
		permissions=admins,
		default_permission=False,
		options=[
			create_option(
				name="module",
				description="The module to set the channel for",
				option_type=3,
				required=True,
				choices=[
					create_choice(
						"minecraft",
						"minecraft"
					),
					create_choice(
						"suggestions",
						"suggestions"
					)
				]
			)
		],
		guild_ids=minecraft_guilds)
	async def slash_set_channel(self, ctx: SlashContext, module: str):
		if module == "minecraft":
			db.set_minecraft_channel(ctx.guild.id, ctx.channel.id)
		elif module == "suggestions":
			db.set_suggestions_channel(ctx.guild.id, ctx.channel.id)
		await ctx.send(f"Set the channel for {module} commands in this server to <#{ctx.channel.id}>", hidden=True)

	@cog_ext.cog_slash(
		name="add-admin",
		description="Gives a role admin permissions for this bot",
		options=[
			create_option(
				name="role",
				description="The role to make an admin",
				option_type=8,
				required=True
			)
		],
		permissions=admins,
		default_permission=False,
		guild_ids=guilds
	)
	async def slash_add_admin_role(self, ctx: SlashContext, role: discord.Role):
		cur_admins = db.get_admin_roles(ctx.guild.id)
		if role.id in cur_admins:
			await ctx.send(f"{role.name} already has admin permissions in this server", hidden=True)
			return
		logging.info(f"Adding {role.id} to admins list")
		cur_admins.append(role.id)
		db.set_admin_roles(ctx.guild.id, cur_admins)
		await ctx.send(f"Added {role.name} to the admins group for {ctx.guild.name}", hidden=True)
		self.bot.reload_extension('admin')

	@cog_ext.cog_slash(
		name="remove-admin",
		description="Removes admin permissions from a role for this bot",
		options=[
			create_option(
				name="role",
				description="The role to remove admin permissions from",
				option_type=8,
				required=True
			)
		],
		permissions=admins,
		default_permission=False,
		guild_ids=guilds
	)
	async def slash_remove_admin_role(self, ctx: SlashContext, role: discord.Role):
		cur_admins = db.get_admin_roles(ctx.guild.id)
		if role.id not in cur_admins:
			await ctx.send(f"{role.name} does not have admin permissions in this server", hidden=True)
			return
		if role.permissions.administrator:
			await ctx.send(f"{role.name} has server admin permissions and can not be removed", hidden=True)
			return
		logging.info(f"Removing {role.id} from the admins list")
		cur_admins.remove(role.id)
		db.set_admin_roles(ctx.guild.id, cur_admins)
		await ctx.send(f"Removed {role.name} from the admins group for {ctx.guild.name}", hidden=True)
		self.bot.reload_extension('admin')

def setup(bot):
    bot.add_cog(Admin(bot))

    