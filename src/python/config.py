import logging
import os
import yaml

logging.getLogger()

class ConfigReader():
    """Helper class for reading config options from environment variables or the config file.
    Prioritizes looking for an environment variable, and if not found will fall back to the config file if present.

    Attributes:
        file_path -- an optional path to a config file in yml format
    """    
    def __init__(self, file_path='config.yml') -> None:
        try:
            with open(file_path, 'r') as doc:
                self.config = yaml.safe_load(doc)
        except FileNotFoundError as e:
            logging.info("Config file not found")
            self.config = {}

    def get(self, key: str) -> str:
        """Read a config key and returns the value or None if the key isn't found

        :param key: Key to look for
        :type key: str
        :return: Value of the key or None
        :rtype: str
        """        
        if os.getenv(key):
            return os.getenv(key)
        else:
            try:
                return self.config[key]
            except AttributeError:
                logging.info("No environment variable found with the specified key and config file not found")
                return None
            except KeyError:
                logging.info(f"{key} was not found in the loaded config file")
                return None
            
    def get_guild_ids(self, key) -> list:
        """Looks for a key and returns a list or None.
        discord.py only supports integer for guild ids, so this function
        ensures that the id object is in the proper format.

        :param key: the key to look for
        :type key: int or str
        :return: returns a list of integers to make discord.py happy
        :rtype: List[int]
        """        
        if os.getenv(key):
            value = os.getenv(key)
            if isinstance(value, str):
                return [int(v) for v in value.split(',')]
            elif isinstance(value, int):
                return [int]
            else:
                raise TypeError(f"Function get_guild_ids expects str or int, got {type(value)}")
        else:
            return None