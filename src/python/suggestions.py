import logging
from re import L
import discord
from discord import member
from discord_slash import cog_ext
from discord_slash.context import SlashContext
from discord_slash.utils.manage_commands import create_choice, create_option
from discord.ext import commands
from config import ConfigReader
from database import DbHelper

logging.getLogger()

config = ConfigReader()
db_path = config.get("db_path")
db = DbHelper(db_path)
guilds = [guild for guild in db.get_guilds()]

read_emoji = "👍"
done_emoji = "✅"
cancel_emoji = "❌"

class Suggestions(commands.Cog):
    
    def __init__(self, bot: commands.Bot) -> None:
        self.bot = bot

    def _build_embed(self, author: str, text: str, type: str, embed_color, link=None) -> discord.Embed:
        embed = discord.Embed(color=embed_color,
                              title=f"New Suggestion by {author}"
                              )
        embed.add_field(name="Suggestion Type", value=type)
        embed.add_field(name="Suggestion", value=text)
        embed.set_author(name=author)
        if link:
            embed.add_field(name="Link", value=link)
        embed.set_footer(text=f"An admin can react to this suggestions with a {read_emoji} to mark it as read\nA {done_emoji} will mark it as complete.\nThe submitter or an admin can react with {cancel_emoji} to close the suggestion.")
        return embed

    @cog_ext.cog_slash(
        name="suggest",
        description="Make a suggestion",
        options=[
            create_option(name="type",
            description="The type of this suggestion",
            option_type=3,
            choices=[
                create_choice(name="Server Suggestion", value="server"),
                create_choice(name="Game Suggestion", value="game"),
                create_choice(name="Emoji Suggestion", value="emoji")
            ],
            required=True),
            create_option(name="suggestion",
            description="Your suggestion",
            option_type=3,
            required=True),
            create_option(name="link",
            description="Optional link for your suggestion",
            option_type=3,
            required=False)
        ], 
        guild_ids=guilds)
    async def slash_suggest(self, ctx: SlashContext, type: str, suggestion: str, link=None):
        link = link or None
        channel_id = db.get_suggestions_channel(ctx.guild.id)
        if not channel_id:
            await ctx.send("No suggestions channel is set. Ask an admin to set the suggestions channel with /set-channel", hidden=True)
            return
        channel = await self.bot.fetch_channel(int(channel_id))
        embed = self._build_embed(author=ctx.author, text=suggestion, type=type, embed_color=discord.Color.light_grey(), link=link)
        await channel.send(embed=embed)
        await ctx.send("Successfully sent suggestion!", hidden=True)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        channel = await self.bot.fetch_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        guild_id = payload.guild_id
        embed = message.embeds[0] # should only have one embed
        fields = {field.name: field.value for field in embed.fields}
        member = payload.member
        emoji = payload.emoji
        admins = db.get_admin_roles(guild_id)
        if channel.id == db.get_suggestions_channel(guild_id) and message.author == self.bot.user: # can only edit own messages
            logging.info(f"{emoji.name} added to {message.id}")
            if emoji.name == read_emoji and [role for role in member.roles if role.id in admins]:
                logging.info(f"Updating embed for {message.id} to read")
                new_embed = self._build_embed(author=embed.author.name, type=fields["Suggestion Type"], text=fields["Suggestion"], embed_color=discord.Color.blue(), link=fields.get("Link", None))
            elif emoji.name == done_emoji and [role for role in member.roles if role.id in admins]:
                logging.info(f"Updating embed for {message.id} to done")
                new_embed = self._build_embed(author=embed.author.name, type=fields["Suggestion Type"], text=fields["Suggestion"], embed_color=discord.Color.green(), link=fields.get("Link", None))
            elif emoji.name == cancel_emoji and [role for role in member.roles if role.id in admins] or member.id == embed.author:
                logging.info(f"Updating embed for {message.id} to cancelled")
                new_embed = self._build_embed(author=embed.author.name, type=fields["Suggestion Type"], text=fields["Suggestion"], embed_color=discord.Color.red(), link=fields.get("Link", None))
            else:
                return
            await message.edit(embed=new_embed)
        else:
            return

def setup(bot):
    bot.add_cog(Suggestions(bot))