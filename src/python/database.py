import sqlite3
import logging

logging.getLogger()

class DbHelper():
    """Assists with database operations

    database is a sqlite3 db file. if none is provided, memory is used.

    if the provided database doesn't exist, it will be created.

    :param database: path to a sqlite3 db file
    :type database: string
    :return: DbHelper instance
    """
    queries = {
        "guild_table": """ CREATE TABLE IF NOT EXISTS guilds (
                            id integer PRIMARY KEY,
                            guildId integer NOT NULL UNIQUE,
                            minecraftChannel integer,
                            suggestionsChannel integer,
                            loggingChannel integer
                        ); """,
        "admins_table": """ CREATE TABLE IF NOT EXISTS admins (
                                id integer PRIMARY KEY,
                                guildId integer NOT NULL UNIQUE,
                                adminRoles text,
                                adminUsers text,
                                FOREIGN KEY (guildId) REFERENCES guilds (guildId)
                        ); """,
        "add_guild": """ INSERT INTO guilds(guildId) VALUES(?) """,
        "add_guild_admin": """ INSERT INTO admins(guildId) VALUES(?) """,
        "get_guilds": """ SELECT guildId FROM guilds """,
        "get_guilds_admin": """ SELECT guildId FROM admins """,
        "get_minecraft_channel": """ SELECT minecraftChannel FROM guilds WHERE guildId = ? """,
        "set_minecraft_channel": """ UPDATE guilds SET minecraftChannel = ? WHERE guildId = ? """,
        "set_admin_roles": """ UPDATE admins SET adminRoles = ? WHERE guildId = ? """,
        "get_admin_roles": """ SELECT adminRoles FROM admins WHERE guildId = ? """,
        "set_suggestions_channel": """ UPDATE guilds SET suggestionsChannel = ? WHERE guildId = ? """,
        "get_suggestions_channel": """ SELECT suggestionsChannel FROM guilds WHERE guildId = ? """
    }

    def __init__(self, database=':memory:'):
        if database == "":
            database = ':memory:'
        self.conn = sqlite3.connect(database)
        self.cur = self.conn.cursor()

    def _extract_list(self, _list: list) -> list:
        """converts a list of tuples to a list

        :param _list: a list of tuples
        :type _list: list
        :return: a list
        :rtype: list
        """        
        return [i for i in _list for i in i] or []


    def initialize(self) -> None:
        """This only needs to be run once, by the main module"""
        logging.info("Bootstrapping tables")
        # this will run through the queries and automatically run any that have "_table" appended to them
        table_queries = [value for key, value in self.queries.items() if "_table" in key]
        for query in table_queries:
            self.cur.execute(query)

    def add_guild(self, guild: int) -> None:
        """Add a guild id to the database

        :param guild: the integer id of the guild
        :type guild: int
        """        
        if guild in self.get_guilds():
            logging.info(f"{guild} already in guilds table")
            return
        logging.info(f"Adding guild {guild} to the guilds table")
        query = self.queries['add_guild']
        self.cur.execute(query, (guild,))
        self.conn.commit()

    def add_guild_admins(self, guild: int):
        if guild in self.get_guild_admins():
            logging.info(f"{guild} already in admins table")
            return
        logging.info(f"Adding guild {guild} to the admins table")
        query = self.queries['add_guild_admin']
        self.cur.execute(query, (guild,))
        self.conn.commit()

    def get_guilds(self) -> list:
        """get the guilds in the database

        :return: list of guild ids
        :rtype: list
        """        
        logging.info("Getting guild IDs")
        query = self.queries["get_guilds"]
        self.cur.execute(query)
        results = self.cur.fetchall()
        return self._extract_list(results)

    def get_guild_admins(self) -> list:
        logging.info("Getting guild IDs in admins table")
        query = self.queries["get_guilds_admin"]
        self.cur.execute(query)
        results = self.cur.fetchall()
        return self._extract_list(results)

    def get_minecraft_channel(self, guild: int) -> int:
        """gets the minecraft channel for the given guild id

        :param guild: guild id
        :type guild: int
        :return: channel int or None
        :rtype: int
        """        
        logging.info(f"Fetching minecraft channel for {guild}")
        query = self.queries['get_minecraft_channel']
        self.cur.execute(query, (guild,))
        result = self.cur.fetchone()
        logging.info(f"The minecraft channel is set to {result[0]}")
        return result[0] # there should really only ever be one result here

    def set_minecraft_channel(self, guild: int, channel: int) -> None:
        """sets the minecraft channel for the given guild id

        :param channel: channel id
        :type channel: int
        :param guild: guild id
        :type guild: int
        """        
        logging.info(f"Setting the minecraft channel for {guild} to {channel}")
        query = self.queries['set_minecraft_channel']
        data = (channel, guild)
        self.cur.execute(query, data)
        self.conn.commit()

    def get_admin_roles(self, guild: int) -> list:
        """Returns a list of admin roles for the given channel

        :param guild: Guild ID to get admins for
        :type guild: int
        :return: List of admin roles
        :rtype: list
        """        
        logging.info(f"Getting admins for guild {guild}")
        query = self.queries['get_admin_roles']
        data = (guild,)
        self.cur.execute(query, data)
        try:
            return [int(i) for i in self.cur.fetchone()[0].split(',')] # result should be a string, so we want to convert to a list
        except (AttributeError, TypeError):
            return []

    def set_admin_roles(self, guild: int, admins: list) -> None:
        """updates the list of admin roles for a given guild id

        :param guild: the guild to update admins for
        :type guild: int
        :param admins: the list of admin roles for the server
        :type admins: list
        """        
        query = self.queries['set_admin_roles']
        data = (','.join(str(i) for i in admins), guild) # admins will be a list, so we want to convert to a string to put in db
        self.cur.execute(query, data)
        self.conn.commit()
    
    def set_suggestions_channel(self, guild: int, channel: int) -> None:
        logging.info(f"Setting the suggestions channel for {guild} to {channel}")
        query = self.queries['set_suggestions_channel']
        data = (channel, guild)
        self.cur.execute(query, data)
        self.conn.commit()

    def get_suggestions_channel(self, guild: int) -> str:
        logging.info(f"Getting suggestions channel for {guild}")
        query = self.queries['get_suggestions_channel']
        data = (guild,)
        self.cur.execute(query, data)
        return self.cur.fetchone()[0]
        