class YabaiError(Exception):
    pass


class TokenError(YabaiError):
    """Exception raised when there is a problem retrieving the login token

    Attributes:
        message -- the message to give with the error
    """

    def __init__(self, message="Discord token not found"):
        self.message = message
        super().__init__(self.message)
