import random

suits = ["Hearts", "Diamonds", "Spades", "Clubs"]
values = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "j", "q", "k", "a"]

def buildDeck() -> list:
    """Takes no parameters and give a set of cards"""
    deck = []
    for suit in suits:
        for value in values:
            deck.append((suit, value))
    return deck

def setup(num_of_decks: int) -> list:
    """Takes a number of decks and returns a list of sets of cards"""
    return [buildDeck() for num in range(num_of_decks)]

def pickCard(decks: list):
    """Takes a list of decks and returns the card"""
    deck = random.choice(decks)
    card = random.choice(deck)
    deck.remove(card)
    if len(deck) == 0:
        decks.remove(deck)
    return card

def cardString(card: tuple) -> str:
    names = {"2": "Two", "3": "Three", "4": "Four", "5": "Five", "6": "Six", "7": "Seven", "8": "Eight", "9": "Nine", "10": "Ten", "j": "Jack", "q": "Queen", "k": "King", "a": "Ace"}
    longName = names[card[1]]
    suitName = card[0]
    return f"{longName} of {suitName}"

def cardsValue(values: list) -> int:
    blackjack = {"2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9, "10": 10, "j": 10, "q": 10, "k": 10, "a": 11}
    total = sum(blackjack[string] for string in values)
    aces = values.count("a")
    if aces:
        for i in range(1, aces):
            if total > 21:
                total = total - 10
    return total

def main(num_of_decks: int, num_of_players: int) -> None:
    deck = setup(num_of_decks)
    # hands will contain all the data about current players and the contents of their hands
    hands = {}
    # totals will contain all the players left in the game and their card totals
    totals = {}
    for i in range(num_of_players + 1):
        hands[i] = (pickCard(deck), [pickCard(deck)])

    for player in hands:
        print(f"Player {player} is showing {cardString(hands[player][0])}")
    for player in hands:
        hit = "y"
        while hit == "y":
            for card in hands[player][1]:
                print(f"Player {player} has {cardString(card)} hidden")
            values = [hands[player][0][1]] + [num for __, num in hands[player][1]]
            total = cardsValue(values)
            print(f"Your total is: {total}")
            hit = input(f"Will {player} draw a card? (y/n) ")
            if hit == "y":
                print(f"Player {player} has drawn a card.")
                hands[player][1].append(pickCard(deck))
                values = [hands[player][0][1]] + [num for __, num in hands[player][1]]
                total = cardsValue(values)
                print(f"Your total is: {total}")
                if total > 21:
                    hit = "n"
                    print(f"Player {player} has busted with {total}.")
        totals[player] = total
    totals_under_21 = {player: total for player, total in totals.items() if total < 21}
    max_val = [keys for keys,values in totals_under_21.items() if values == max(totals_under_21.values())]
    for player, value in totals.items():
        print(f"{player} has {value}")
    for player in max_val:
        print(f"{player} has won!")

main(5, 2)
