import logging
from discord_slash import cog_ext
from discord_slash.context import SlashContext
from discord_slash.utils.manage_commands import create_option
from mctools import RCONClient, errors
from discord.ext import commands
from config import ConfigReader
from database import DbHelper

logging.getLogger()

config = ConfigReader()
guilds = config.get_guild_ids('minecraft_guilds')
db_path = config.get("db_path")

class Minecraft(commands.Cog):
	def __init__(self, bot: commands.Bot):
		self.bot = bot
		self.password = config.get('minecraft_rcon_pass')
		self.host = config.get('minecraft_host_name')
		self.db = DbHelper(db_path)

	def _connect(self):
		client = RCONClient(self.host)

		if client.login(self.password):
			return client
		else:
			raise errors.RCONAuthenticationError

	def _sendCommand(self, command: str):
		client = self._connect()
		response = client.command(command)
		client.stop()
		return response[:-4]

	@cog_ext.cog_slash(
		name="whitelist",
		description="Whitelist yourself on the Minecraft server",
		options=[
			create_option(
				name="username",
				description="Your mojang account name (it appears in the top left of the launcher)",
				option_type=3,
				required=True
			)
		],
		guild_ids=guilds)
	async def slash_whitelist(self, ctx: SlashContext, username: str):
		author = ctx.author
		minecraft_channel = self.db.get_minecraft_channel(ctx.guild.id)
		if ctx.channel.id != minecraft_channel and minecraft_channel != None:
			await ctx.send(f"This is not the proper channel for minecraft commands. Please use <#{minecraft_channel}>", hidden=True)
			return
		await ctx.send(f"{author} requested {username} be added to the whitelist")
		command = f"whitelist add {username}"
		response = self._sendCommand(command)
		await ctx.send(response)

	# @commands.command(name="whitelist", help="Whitelist yourself in the Minecraft server")
	# async def whitelist(self, ctx, username: str):
	# 	author = ctx.author
	# 	await ctx.send(f"{author} requested {username} be added to the whitelist")
	# 	command = f"whitelist add {username}"
	# 	response = self._sendCommand(command)
	# 	await ctx.send(response)

	# @commands.command(name="show", help="Shows various information about the minecraft server")
	# async def show(self, ctx, *, message):
	# 	"""Parses message and returns the requested information

	# 	:param ctx: the message context  
	# 	:param message: the message content  
	# 	:type ctx: discord.ext.commands.Context  
	# 	:type message: string
	# 	"""
	# 	comms = {
	# 		"online users": {"pre_message": "Retrieving list of users", "command": "list"},
	# 		"whitelisted users": {"pre_message": "Retrieving list of whitelisted users", "command": "whitelist list"},
	# 		"status": {"pre_message": "Getting status of server", "command": "seed"}
	# 	}
	# 	try:
	# 		selected = comms[message]
	# 	except KeyError:
	# 		await ctx.send("I don't know what you're asking me to look for")
	# 		return
	# 	await ctx.send(selected['pre_message'])
	# 	response = self._sendCommand(selected['command'])
	# 	await ctx.send(response)
		
	# Members that have perms to ban members also have perms to dewhitelist users from the minecraft server
	# @commands.command(name="dewhitelist", help="De-whitelist a user from the Minecraft server")
	# @commands.has_permissions(ban_members=True)
	# async def dewhitelist(self, ctx, username: str):
	# 	await ctx.send(f"Removing {username} from the whitelist")
	# 	command = f"whitelist remove {username}"
	# 	response = self._sendCommand(command)
	# 	await ctx.send(response)

	# @dewhitelist.error
	# async def dewhitelist_error(self, error, ctx):
	# 	if isinstance(error, 'MissingPermissions'):
	# 		await self.bot.send_message(ctx.message.channel, "You do not have permission to de-whitelist users")
	# 	else:
	# 		raise error

def setup(bot):
    bot.add_cog(Minecraft(bot))