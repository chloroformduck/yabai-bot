param (
  [string]$Token = $( Read-Host "Input Discord token" ),
  [string]$Rcon_host = $( Read-Host "Input RCON host" ),
  [string]$Rcon_pass = $( Read-Host "Input RCON password" ),
  [string]$Guild = $( Read-Host "Input Guild Id"),
  [string]$Dbpath = $( Read-Host "Input the sqlite db path in the docker container (should be /storage/somefile.db)")
)

$docker = Get-Process docker -ErrorAction SilentlyContinue

if (!$docker) {
  Write-Host "Docker is not running. Please start docker desktop before running this script."
  Exit 1
}

try{
  docker build -t yabai .
  docker run -d -v ${PSScriptRoot}\src\python:/storage -e db_path=$Dbpath -e guilds=$Guild -e minecraft_guilds=$Guild -e credentials_token=$Token -e minecraft_host_name=$Rcon_host -e minecraft_rcon_pass=$Rcon_pass --name "YabBot" yabai
  Read-Host "Press enter to clean up docker container"
}
finally{
  docker rm -f YabBot
}
